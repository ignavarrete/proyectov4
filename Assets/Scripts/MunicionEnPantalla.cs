﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MunicionEnPantalla : MonoBehaviour
{
    public Text texto;
    public FuncionArma funcionArma;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        texto.text = "Municion:\n" + funcionArma.municion + "/" + funcionArma.tamañoCargador + "\n" + funcionArma.municionRestantes;

    }
}
