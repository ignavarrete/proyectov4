﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    private GameObject Player;

    private Timer timer;

    [SerializeField]
    private GameObject objetivo;

    private GameObject objetivoEncontrado;

    //[SerializeField]
    //private GameObject menuCamera;

    //[SerializeField]
    //private GameObject menuUI;

    //[SerializeField]
    //private GameObject gameUI;

    // Start is called before the first frame update
    void Start()
    {
        timer = gameObject.GetComponent<Timer>();

        //menuCamera.SetActive(true);
        //menuUI.SetActive(true);
        //gameUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void startGame()
    {
        timer.startTimer();
        //menuCamera.SetActive(false);
        //menuUI.SetActive(false);
        //gameUI.SetActive(true);
    }

    public void endGame()
    {
        timer.stopTimer();
        Destroy(Player);
        //menuCamera.SetActive(true);
        //menuUI.SetActive(true);
        //gameUI.SetActive(false);
    }
}
